package app.hw3.model;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Product
 *
 */
public class Product implements Serializable {

	@Id
	private Integer barcode;
	private String name;
	private String color;
	private String description;
	private static final long serialVersionUID = 1L;

	public Product() {
		super();
	}
	
	public Product(Integer barcode, String name, String color, String description) {
		super();
		setBarcode(barcode);
		setName(name);
		setColor(color);
		setDescription(description);
	}

	public Integer getBarcode() {
		return this.barcode;
	}

	public void setBarcode(Integer barcode) {
		this.barcode = barcode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
