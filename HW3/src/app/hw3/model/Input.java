package app.hw3.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class Input {
	public static Integer maxBarcodeValue = Integer.MAX_VALUE;
	public static Integer maxBarcodeLength = (int) Math.ceil(Math.log10(maxBarcodeValue));
	public static Integer maxNameLength = 20;
	public static Integer maxColorLength = 20;
	public static Integer descriptionRows = 5;
	public static Integer descriptionColumns = 20;
	public static Integer maxDescriptionLength = descriptionRows * descriptionColumns;

	public static ArrayList<String> validate(String barcode, String name, String color, String description) {
		ArrayList<String> errors = new ArrayList<String>();

		if (name.isEmpty()) {
			errors.add("The given name field is empty.");
		} else if (name.length() > maxNameLength) {
			errors.add("The given name is longer than " + maxNameLength + " characters");
		}

		if (color.isEmpty()) {
			errors.add("The given color field is empty.");
		} else if (color.length() > maxColorLength) {
			errors.add("The given color is longer than " + maxColorLength + " characters");
		}

		if (description.isEmpty()) {
			errors.add("The given description field is empty.");
		} else if (description.length() > maxDescriptionLength) {
			errors.add("The given description is longer than " + maxDescriptionLength + " characters");
		}

		if (barcode.isEmpty()) {
			errors.add("The given barcode field is empty.");
		} else if (!Pattern.matches("\\d+", barcode)) {
			errors.add("The given barcode is not a non-negative integer.");
		} else if (barcode.length() > maxBarcodeLength) {
			errors.add("The given barcode is longer than " + maxBarcodeLength + " characters");
		} else if ((new BigInteger(barcode)).compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) > 0) {
			errors.add("The given barcode is greater than " + maxBarcodeValue);
		} else if (Database.productAlreadyAdded(Integer.parseInt(barcode))) {
			errors.add("A product with the same barcode is already in the database.");
		}

		return errors;
	}
}
