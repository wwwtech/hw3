package app.hw3.model;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Database {
	public static Integer maxProducts = 100;

	public static EntityManager getEntityManager() {
		return Persistence.createEntityManagerFactory("HW3").createEntityManager();
	}

	private static Product findProduct(Integer barcode) {
		EntityManager em = getEntityManager();
		Query q = em.createQuery("SELECT p FROM Product p WHERE p.barcode = :b");
		q.setParameter("b", barcode);
		@SuppressWarnings("unchecked")
		List<Product> products = q.getResultList();
		Product product = products.isEmpty() ? null : products.get(0);
		em.close();
		return product;
	}

	public static List<Product> getProducts() {
		EntityManager em = getEntityManager();
		Query q = em.createQuery("SELECT p FROM Product p ORDER BY p.barcode");
		@SuppressWarnings("unchecked")
		List<Product> products = q.getResultList();
		em.close();
		return products;
	}

	public static Integer getProductCount() {
		EntityManager em = getEntityManager();
		Query q = em.createQuery("SELECT COUNT(p) FROM Product p");
		Long count = (Long) q.getSingleResult();
		em.close();
		return count.intValue();
	}

	public static Boolean isFull() {
		return getProductCount() >= maxProducts;
	}

	public static Boolean productAlreadyAdded(Integer barcode) {
		return findProduct(barcode) != null;
	}

	public static void addProduct(Product p) {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		em.persist(p);
		em.getTransaction().commit();
		em.close();
	}
}