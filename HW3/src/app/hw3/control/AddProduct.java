package app.hw3.control;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.hw3.model.Database;
import app.hw3.model.Input;
import app.hw3.model.Product;

@WebServlet(description = "Handles addition of products into the database", urlPatterns = { "/AddProduct" })
public class AddProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String barcode = request.getParameter("product_barcode").trim();
		String name = request.getParameter("product_name").trim();
		String color = request.getParameter("product_color").trim();
		String description = request.getParameter("product_description").trim();

		try {
			ArrayList<String> errors = Input.validate(barcode, name, color, description);
			if (!errors.isEmpty()) {
				request.setAttribute("errors", errors);
				RequestDispatcher rd = request.getRequestDispatcher("product_addition_failure.jsp");
				rd.forward(request, response);
				System.out.println("Product addition failed due to user input errors.");
				return;
			}

			if (Database.isFull()) {
				errors.add("The database is full.");
				request.setAttribute("errors", errors);
				RequestDispatcher rd = request.getRequestDispatcher("product_addition_failure.jsp");
				rd.forward(request, response);
				System.out.println("Product addition failed because the database is full.");
				return;
			}

			Database.addProduct(new Product(Integer.parseInt(barcode), name, color, description));
			RequestDispatcher rd = request.getRequestDispatcher("product_addition_success.jsp");
			rd.forward(request, response);
			System.out.println("Product addition succeeded.");
		} catch (Exception e) {
			RequestDispatcher rd = request.getRequestDispatcher("unknown_failure.jsp");
			rd.forward(request, response);
			System.out.println(e.getMessage());
		}
	}
}
