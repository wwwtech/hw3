<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="include/head.html"%>
<%!String title = "Homework 3";%>
<title><%=title%></title>
</head>
<body>
	<h1><%=title%></h1>
	<%@include file="include/footer.html"%>
</body>
</html>