<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="include/head.html"%>
<title>Product Addition Success</title>
</head>
<body>
	<h1>Product Addition Success</h1>
	<h2>Successfully added the following product</h2>
	<%@ include file="include/product.jsp"%>
	<%@ include file="include/footer.html"%>
</body>
</html>