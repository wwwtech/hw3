<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.ArrayList,java.util.Iterator"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="include/head.html"%>
<title>Product Addition Failure</title>
</head>
<body>
	<h1>Product Addition Failure</h1>

	<h2>Failed to add the following product</h2>
	<%@ include file="include/product.jsp"%>

	<h2>Errors</h2>
	<ul class="error-list">
		<%
			@SuppressWarnings("unchecked")
			ArrayList<String> errors = (ArrayList<String>) request.getAttribute("errors");
			Iterator<String> it = errors.iterator();
			while (it.hasNext()) {
				out.print("<li class='error'>" + it.next() + "</li>");
			}
		%>
	</ul>

	<%@ include file="include/footer.html"%>
</body>
</html>