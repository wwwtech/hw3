<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="app.hw3.model.Input"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="include/head.html"%>
<%!String title = "Add New Product";%>
<title><%=title%></title>
</head>
<body>
	<h1><%=title%></h1>
	<form method="POST" action="AddProduct">
		<div class="container rows">
			<div class="row">
				<div class="col field-name">Barcode</div>
				<div class="col field-value">
					<input type="text" name="product_barcode" data-toggle="tooltip"
						data-placement="right"
						title="Type a non-negative integer of at most <%=Input.maxBarcodeLength%> digits (0 to <%=Input.maxBarcodeValue%>)." />
				</div>
			</div>
			<div class="row">
				<div class="col field-name">Name</div>
				<div class="col field-value">
					<input type="text" name="product_name" data-toggle="tooltip"
						data-placement="right"
						title="Type at most <%=Input.maxNameLength%> characters" />
				</div>
			</div>
			<div class="row">
				<div class="col field-name">Color</div>
				<div class="col field-value">
					<input type="text" name="product_color" data-toggle="tooltip"
						data-placement="right"
						title="Type at most <%=Input.maxColorLength%> characters" />
				</div>
			</div>
			<div class="row">
				<div class="col field-name">Description</div>
				<div class="col field-value">
					<textarea name="product_description"
						rows="<%=Input.descriptionRows%>"
						cols="<%=Input.descriptionColumns%>" data-toggle="tooltip"
						data-placement="right"
						title="Type at most <%=Input.maxDescriptionLength%> characters"></textarea>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<input type="submit" value="Add">
				</div>
			</div>
		</div>
	</form>

	<%@ include file="include/footer.html"%>
</body>
</html>