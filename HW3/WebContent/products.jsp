<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="app.hw3.model.Database,java.util.Iterator,app.hw3.model.Product"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="include/head.html"%>
<%!String title = "Products";%>
<title><%=title%></title>
</head>
<body>
	<h1><%=title%></h1>
	<div class="subtitle">
		The database currently contains
		<%=Database.getProductCount()%>
		products
	</div>
	<div class="container columns">
		<div class='row header'>
			<div class="col field-name numeric">Barcode</div>
			<div class="col field-name">Name</div>
			<div class="col field-name">Color</div>
			<div class="col field-name">Description</div>
		</div>
		<%
			Iterator<Product> it = Database.getProducts().iterator();
			while (it.hasNext()) {
				Product p = it.next();
				Integer b = p.getBarcode();
				String n = p.getName();
				String c = p.getColor();
				String d = p.getDescription();
				out.print("<div class='row'>");
				out.print(String.format("<div class='col field-value numeric'>%d</div>\n", b));
				out.print(String.format("<div class='col field-value'>%s</div>\n", n));
				out.print(String.format("<div class='col field-value'>%s</div>\n", c));
				out.print(String.format("<div class='col field-value'>%s</div>\n", d));
				out.print("</div>");
			}
		%>
	</div>

	<%@ include file="include/footer.html"%>
</body>
</html>